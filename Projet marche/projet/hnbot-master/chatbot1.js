class martha{
  constructor(username){
    var RiveScript = require("rivescript");
    this.bot = new RiveScript();
    this.user = username;
  }

  update(cerveau){
    this.bot.loadDirectory(cerveau);
  }

  run(cerveau){
    var express = require('express');
    var app = express();
    var router = express.Router();
    var http = require('http').Server(app);
    var io = require('socket.io')(http);
    var fs = require('fs');
    var cbot = this.bot;
    var username = this.user;

    this.bot.loadDirectory(cerveau);

    app.use(express.static('assets'));

    app.get('/', function (req, res) {
      res.sendFile(__dirname + '/chatbot1.html'
    );
    });

    io.on('connection', function (socket) {
      socket.on('chat message', function (msg) {
        cbot.sortReplies();
        // And now we're free to get a reply from the brain!
        console.log("Input received: " + msg);
        fs.appendFile('conversation/'+username+'.txt','Chat message :  '+msg+'\n', function(err) {
          // If an error occurred, show it and return
          if(err) return console.error(err);
          // Successfully wrote to the file!
        });

        var reply = cbot.reply("local-user", msg);
        io.emit('chat message', reply);
        console.log("Bot response: " + reply);
        fs.appendFile('conversation/'+username+'.txt','Bot response :  '+reply+'\n', function(err) {
          // If an error occurred, show it and return
          if(err) return console.error(err);
          // Successfully wrote to the file!
        });

      });
    });


    http.listen(3001, function () {
      console.log('Martha started listening on port : 3001');
    });
  }
}
module.exports = martha;
