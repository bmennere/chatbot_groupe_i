class john{

  constructor(){
    var RiveScript = require("rivescript");
    this.bot = new RiveScript();
  }

  update(cerveau){
    this.bot.loadDirectory(cerveau);
  }

  run(cerveau){
    var express = require('express');
    var app = express();
    var router = express.Router();
    var http = require('http').Server(app);
    var io = require('socket.io')(http);
    // var RiveScript = require("rivescript");
    // this.bot = new RiveScript();
    var cbot = this.bot;

    this.bot.loadDirectory(cerveau);

    app.use(express.static('assets'));

    app.get('/', function (req, res) {
      res.sendFile(__dirname + '/chatbot3.html'
      );
    });

    io.on('connection', function (socket) {
      socket.on('chat message', function (msg) {
        cbot.sortReplies();

        // And now we're free to get a reply from the brain!
        console.log("Input received: " + msg);
        var reply = cbot.reply("local-user", msg);
        io.emit('chat message', reply);
        console.log("Bot response: " + reply);
      });
      //io.close();
    });

    // router.post('/quit', function(){
    //   io.close();
    //   console.log("John stopped listening");
    // });


    http.listen(3003, function () {
      console.log('John started listening on port : 3003');
    });
  }
}
module.exports = john;
