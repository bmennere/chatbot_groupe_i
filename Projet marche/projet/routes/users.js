var express = require('express');
var router = express.Router();
var login = require('../controller/authenticate/login');
var app = express();

var chatbot1 = require('./../hnbot-master/chatbot1.js');
var chatbot2 = require('./../hnbot-master/chatbot2.js');
var chatbot3 = require('./../hnbot-master/chatbot3.js');

flagsteeve=true;
flagmartha=true;
flagjohn=true;


/* GET users listing. */
router.get('/', function (req, res, next) {
    res.send('respond with a resource');
});
/* Login user */
router.post('/login', function (req, res, next) {
const username = req.body.username;
    let loginResult = login(username, req.body.password);
if (!loginResult) {
        //Chatbots running
        bot1 = new chatbot1(username);
        bot2 = new chatbot2(username);
        bot3 = new chatbot3(username);
        //Socket initialize with standard brain
        bot1.run("hnbot-master/brain/Standard");
        bot2.run("hnbot-master/brain/Standard");
        bot3.run("hnbot-master/brain/Standard");

        res.render('accueil', {username: username, steeve: flagsteeve, martha: flagmartha, john: flagjohn});
    }
    else {
        res.render('index', {error: true});
    }
});

router.post('/accueil', function (req, res, next) {
  res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/signup', function (req, res, next) {
        res.render('signup');
});

router.post('/signout', function (req, res, next) {
        res.render('index');
});

router.post('/martha', function (req, res, next) {

    var opn = require('opn');
    // opens the url in the default browser
    opn('http://localhost:3001/');
});

router.post('/steeve', function (req, res, next) {

    var opn = require('opn');
    // opens the url in the default browser
    opn('http://localhost:3002/');
});

router.post('/john', function (req, res, next) {

    var opn = require('opn');
    // opens the url in the default browser
    opn('http://localhost:3003/');
});

router.post('/modifiersteeve', function (req, res, next) {
        res.render('modifiersteeve');
});

router.post('/supprimersteeve', function (req, res, next) {
        flagsteeve = false;
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/ajoutersteeve', function (req, res, next) {
        flagsteeve = true;
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/modifiermartha', function (req, res, next) {
        res.render('modifiermartha');
});

router.post('/supprimermartha', function (req, res, next) {
        flagmartha = false;
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/ajoutermartha', function (req, res, next) {
        flagmartha = true;
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/modifierjohn', function (req, res, next) {
        res.render('modifierjohn');
});

router.post('/supprimerjohn', function (req, res, next) {
        flagjohn = false;
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/ajouterjohn', function (req, res, next) {
        flagjohn = true;
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});


/* Modification des cerveaux des bots */
router.post('/standardjohn', function (req, res, next) {
        bot3.update("hnbot-master/brain/Standard");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/improvedjohn', function (req, res, next) {
        bot3.update("hnbot-master/brain/Improved");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});
router.post('/premiumjohn', function (req, res, next) {
        bot3.update("hnbot-master/brain/Premium");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});


router.post('/standardsteeve', function (req, res, next) {
        bot2.update("hnbot-master/brain/Standard");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/improvedsteeve', function (req, res, next) {
        bot2.update("hnbot-master/brain/Improved");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});
router.post('/premiumsteeve', function (req, res, next) {
        bot2.update("hnbot-master/brain/Premium");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});


router.post('/standardmartha', function (req, res, next) {
        bot1.update("hnbot-master/brain/Standard");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

router.post('/improvedmartha', function (req, res, next) {
        bot1.update("hnbot-master/brain/Improved");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});
router.post('/premiummartha', function (req, res, next) {
        bot1.update("hnbot-master/brain/Premium");
        res.render('accueil', {steeve: flagsteeve, martha: flagmartha, john: flagjohn});
});

module.exports = router;
